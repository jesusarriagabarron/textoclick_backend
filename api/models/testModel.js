'use strict'

var mongoose  = require('mongoose');
var Schema    = mongoose.Schema;

var Respuesta = new Schema({
  respuesta: {
    type: String,
    required: true
  },
  correcto:{
    type: Boolean,
    default: false
  }
});

var Pregunta = new Schema({
  pregunta: {
    type: String,
    required: true,
  },
  respuestas: [ Respuesta ]
});

var TestSchema = new Schema({
  title:{
    type: String,
    required: true
  },
  limit:{
      type: Number,
      default: 10
  },
  badge: {
    image: {
      type: String,
      required: true,
    },
    image_blocked: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true
    },
  },
  preguntas: [ Pregunta ]

},{timestamps: true});

module.exports = mongoose.model('Test',TestSchema);
