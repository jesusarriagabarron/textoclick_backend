'use strict'

var mongoose  = require('mongoose');
var Schema    = mongoose.Schema;


var ArticleCategorySchema = new Schema( {
  title:{
    type: String,
    require: true,
    unique: true
  },
  active:{
    type:Boolean,
    default:true,
  },
},{ timestamps: true } );

module.exports = mongoose.model('ArticleCategory',ArticleCategorySchema);
