'use strict'

var mongoose  = require('mongoose');
var Schema    = mongoose.Schema;

var Test = new Schema({
    _id:{
        required:true,
        unique:true,
        type: mongoose.Schema.Types.ObjectId
    },
    chances:[{date:Date,_id : false }],
    approved:{
        required: true,
        type: Boolean,
        default: false
    },
    initiate:{
        required: true,
        type:Boolean,
    },
    initiateTime:{
        type: Date,
    },
    finalTime:{
        type:Date,
    }
});

var RedactorSchema = new Schema({
  _id:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    unique: true
  },
  points:{
    type: Number,
    default:0
  },
  badges:[],
  rating:{
    type:Number,
    default: 0
  },
  tests:[Test],
  active:{
    type:Boolean,
    default:false,
  },
},{timestamps: true});

module.exports = mongoose.model('Redactor',RedactorSchema);
