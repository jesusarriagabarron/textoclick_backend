'use strict'

var mongoose  = require('mongoose');
var Schema    = mongoose.Schema;


var ArticleSchema = new Schema({
  title:{
    type: String,
    required: 'Title is required',
    index: true
  },
  redactor:{
    _id: mongoose.Schema.Types.ObjectId,
    firstName: String,
    lastName: String,
    country: String
  },
  views:{
    type: Number
  },
  price:{
    type: Number,
    required: true,
  },
  category:[],
  words:{
    type: Number,
    required: true,
  },
  images:{
    type: Number,
    default: 0
  },
  filename:{
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true,
    lowercase: true
  }
},{timestamps: true});

module.exports = mongoose.model('Article',ArticleSchema);
