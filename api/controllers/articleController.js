'use strict'

var ArticleCategory = require('../models/articleCategoryModel'),
    Article = require('../models/articleModel'),
    Redactor = require('../models/redactorModel'),
    path = require('path'),
    multer = require('multer'),
    storage = require('../helpers/storage.js'),
    slugify = require('slugify'),
    randomstring = require('randomstring'),
    fs = require('fs');



//=================== mimetype filter for multer  DOC DOCX
var upload = multer({
    dest: 'uploads/',
    fileFilter: function (req, file, cb) {
        var filetypes = /doc|docx/;
        var mimetype = filetypes.test(file.mimetype);
        var extname = filetypes.test(path.extname(file.originalname).toLowerCase());
        if (mimetype && extname) {
            return cb(null, true);
        }
        cb("invalid-extension");
    }
}).single('fileupload');



//========================== Method to upload a new article

exports.upload_article = (req, res) => {
    upload(req, res, err => {
        if (err && err == "invalid-extension") return res.status(400).json({
            error: 'invalid-extension'
        });
        if (err) return console.log(err);
        if (req.file) {
            //if a file was uploaded validate that redactor is certified
            Redactor.findOne({
                _id: req.jwt._id,
                'tests.approved': true
            }, (err, _redactor) => {

                if (_redactor) {
                    let filename = randomstring.generate(5) + '-' + slugify(req.file.originalname);
                    storage.save_document(`./${req.file.path}`, req.jwt._id, filename,
                        (err, data) => {
                            fs.unlinkSync(`./${req.file.path}`);
                            if (err) {
                                console.log(err);
                                return res.status(400).json({
                                    error: 'upload-failed'
                                });
                            };
                            //TODO create a article with complete data
                            let article = new Article({
                                title: req.body.title,
                                redactor: {
                                    _id: req.jwt._id,
                                    firstName: req.jwt.firstName,
                                    lastName: req.jwt.lastName,
                                    country: req.jwt.country
                                },
                                views: 0,
                                price: parseFloat(req.body.price).toFixed(2),
                                category: JSON.parse(req.body.categories),
                                images: req.body.images,
                                words: req.body.words,
                                filename: filename,
                                status: 'published'
                            });

                            article.save((err, _article) => {
                                if (err) {
                                    console.log(err);
                                    return res.status(400).json({
                                        error: err
                                    });
                                };
                                res.json({
                                    success: 'article-upload'
                                });
                            });
                        });
                } else {
                    fs.unlinkSync(`./${req.file.path}`);
                    res.status(400).json({
                        error: 'redactor-disabled'
                    });
                }
            });
        } else {
            res.status(400).json({
                error: 'upload-no-selected'
            });
        }

    }); //upload end
};


//========================== Method to get article categories
exports.get_categories = (req, res) => {
    //search for user id in redactor collection
    ArticleCategory.find({}, (err, categories) => {
        if (err) return console.log(err);
        res.json({
            categories: categories
        });
    });
};

//======================== Method to get article Info
exports.get_article = (req, res) => {
    Article.findOne({
        _id: req.params.id
    }, (err, _article) => {
        if (err) {
            console.log(err);
            return res.status(400).json({
                error: err
            });
        };
        let articleSelected = _article.toObject();
        return res.json({
            success: 'ok',
            article: articleSelected
        });
    });
}
//======================== Method to get the Redactor's Articles
exports.get_my_articles = (req, res) => {
    Article.find({
        'redactor._id': req.jwt._id
    }, (err, articles) => {
        if (err) {
            console.log(err);
            return res.status(400).json({
                error: err
            });
        };
        return res.json({
            success: 'ok',
            articles: articles
        });
    });
};

//======================== Download my Article  only for owner
exports.download_article_private = (req, res) => {
    Article.findOne({
        _id: req.params.id,
        'redactor._id': req.jwt._id
    }, (err, _article) => {
        if (err) {
            console.log(err);
            return res.status(400).json({
                error: err
            });
        };
        let articleSelected = _article.toObject();
        let filename = `documents/${req.jwt._id}/${articleSelected.filename}`;
        //get Object storage
        storage.getObject(filename)
            .then( response =>{
                //send header file and send binary 
                res.setHeader('Content-Disposition', 'attachment; filename=' + articleSelected.filename);
                res.setHeader('Content-Transfer-Encoding', 'binary');
                res.setHeader('Content-Type', 'application/octet-stream');
                res.end(response.Body,'binary');
            });
    });
}