'use strict'
var Test              = require('../models/testModel'),
    Redactor          = require('../models/redactorModel'),
    _                 = require('lodash'),
    moment            = require('moment');

//=== function to get questions and shuffle them from specific test
var getQuestions = function( _test ){
    let preguntas = [];
    _.forEach( _test.preguntas, ( p ) => {
        let preguntaSolo =  p.toObject()
        _.forEach( preguntaSolo.respuestas, ( value, key )  => {
            preguntaSolo.respuestas[key] = _.omit( value, ['correcto'] );
        });
        preguntaSolo.respuestas = _.shuffle(preguntaSolo.respuestas);
        preguntas.push( preguntaSolo );
        preguntas = _.shuffle(preguntas);
    });
    return preguntas;
}
//=== function to get answers of a test
var getAnswers = function ( _test ){
    let respuestas = [];
    _.forEach( _test.preguntas, ( p ) => {
        let preguntaSolo =  p.toObject()
        _.forEach( preguntaSolo.respuestas, ( value, key )  => {
            if(value['correcto']==true){
                respuestas.push(value['_id']);
            }
        });
    });
    return respuestas;
}



//========================== Method to finish and review the TEST certification
exports.finish_test = (req, res) => {
    console.log(' reviewing ceritifcation '+req.body.id);
    let userAnswers = req.body.answers;
    let totalAnswers = req.body.answers.length;
    let correctAnswers =0;
    let answers = [];

    //find the Redactor
    Redactor.findOne({
        _id: req.jwt._id,
        'tests._id':req.body.id,
        'tests.initiate':true,
        'tests.approved':false
    }, ( err, _redactor ) => {
        if( _redactor ){
            //if test is initiated and not approved for that redactor
            //find the test to review
            Test.findOne({ _id: req.body.id }, ( err, _test ) => {
                if(err) return console.log("****************"+err);
                if( _test ){
                    //check if number of answers match with total answers in test
                    if( _test.preguntas.length == totalAnswers){
                         answers = getAnswers( _test );
                         correctAnswers = 0;
                         //review answers
                        for(var i=0; i< answers.length; i++ ){
                            for(var j=0; j<userAnswers.length;j++){
                                    if(userAnswers[j]==answers[i]){
                                        correctAnswers++;
                                        break;
                                    }
                            }
                        }
                        let approved =  ( correctAnswers == answers.length )?true:false;
                        let setQuery = { 'tests.$.initiate': false, 'tests.$.approved':approved };
                        if(approved){ setQuery = { 'tests.$.initiate': false, 'tests.$.approved':approved, 'tests.$.finalTime':new Date() };}
                        // If Approved then save the user has approved into the system....!
                        //update the chances and status for that test and redactor record
                        Redactor.findOneAndUpdate(
                            { _id: req.jwt._id,'tests._id':req.body.id, },
                            {
                                $push : { 'tests.$.chances': {"date": new Date() } },
                                $set  : setQuery,
                            },
                            function(err, _record){
                                if(err) return console.log("***************"+err);
                                let chances = _record.tests.id(req.body.id).chances.length;
                                res.json({
                                    success:'reviewing and finishing',
                                    result: correctAnswers,
                                    totalQuestions:answers.length,
                                    approved:approved,
                                    chances: chances+1
                                });
                            }//end callback findOneAndUpdate
                        );
                    }else{
                        res.status(400).json({error:'error al enviar las respuestas'});
                    }
                }
            });

        }else{
            console.log('error redactor o test no encontrado');
            res.status(400).json({error:'redactor-notfound'});
        }
    });
};




//========================== Method to get an specific test
exports.get_test = ( req, res ) => {
    //get the redactor
    Redactor.findOne({ _id: req.jwt._id ,'tests._id':req.params.id }, ( err, _redactor ) => {
        //if exists verify the
        if(_redactor){
            //verifify is not initiate
            if( _redactor.tests.id(req.params.id).initiate == true    ){
                return res.status(400).json({ error:'test-previousInitiate' });
            }
            //verify is not approved
            if(_redactor.tests.id(req.params.id).approved == true ){
                return res.status(400).json({ error:'test-wasApproved' });
            }
            //verify was not previous taken
            let chanceToday = _redactor.tests.id(req.params.id).chances.filter( ele => moment(ele.date).format('YYYY-MM-DD') ==  moment().format('YYYY-MM-DD'));
            if(chanceToday.length>0){   return res.status(400).json({error:'test-chance'})  }
        }

        Test.findOne({ _id: req.params.id }, ( err, _test ) => {
            if(err) return console.log("****************"+err);
            if(_test){
                    _test = _test.toObject();
                    delete _test.preguntas;
            }
            res.json( { test: _test } );
        });

    });
};




//========================== Method to start certification
exports.start_test = ( req, res ) => {
    //find if the user has a redactor record
    Redactor.findOne({ _id: req.jwt._id }, ( err, _redactor ) => {
        if(err) return console.log("****************"+err);
        //if redactor exists
        if( _redactor ){
            let testToStart = _redactor.tests.id(req.body.id);
            if(testToStart){
                if(testToStart.approved == true ) {   return  res.status(400).json({error:'test-approved'}) }
                if(testToStart.initiate == true ) {   return  res.status(400).json({error:'test-initiate'}) }
                let chanceToday = testToStart.chances.filter( ele => moment(ele.date).format('YYYY-MM-DD') ==  moment().format('YYYY-MM-DD'));
                if(chanceToday.length>0){   return res.status(400).json({error:'test-chance'})  }

                Test.findOne({ _id: req.body.id }, ( err, _test ) => {
                    if(_test){
                        testToStart.initiate = true;
                        testToStart.initiateTime = new Date();
                        _redactor.save(function(err, tr) {
                            res.json( { success: 'test-running', questions: getQuestions( _test ) });
                        });
                    }else{
                        res.status(400).json({error:'test-notfound'});
                    }
                });
            }else{
                //validate that tests exists
                Test.findOne({ _id: req.body.id }, ( err, _test ) => {
                    if(_test){
                        _redactor.tests.push({ _id: _test._id, initiate: true,
                                                approved: false, initiateTime: new Date() });
                        _redactor.save(function(err, tr) {
                            res.json( { success: 'test-running', questions: getQuestions( _test ) });
                        });
                    }else{
                        res.status(400).json({error:'test-notfound'});
                    }
                });
            }
        }else{
            // New redactor file , find the test and start it
            Test.findOne({ _id: req.body.id }, ( err, _test ) => {
                if(err) return console.log("****************"+err);
                if( _test ){
                    //create new redactor record
                    let redactor = new Redactor({
                            _id: req.jwt._id,
                            //init new test record
                            tests: [{
                                _id: _test._id,
                                initiate: true,
                                approved: false,
                                initiateTime: new Date()
                            }],
                            active: false,
                            rating: 0,
                            points: 0
                        });
                    redactor.save( ( err, _redactor ) => {
                        if(err) return console.log("****************"+err);
                        if( _redactor ){
                            res.json({success: 'test-running', questions: getQuestions( _test ) });
                        }
                    });
                }
            });
        }
    });

};
//========================== Method to get all Tests
exports.all_test = ( req, res ) => {
    let arrayTests = [];
    Test.find({ }, ( err, _tests ) => {
        if(err) return console.log("****************"+err);
        if(_tests){
            _.forEach( _tests , index => {
                index = index.toObject();
                delete index.preguntas;
                arrayTests.push( index );
            } );
        }
        //set chances to zero and approved false by default , unless it was found on redactor table
        for( var i=0; i<arrayTests.length; i++ ){
            arrayTests[i].chances = 0;
            arrayTests[i].approved = false;
        }
        Redactor.findOne({ _id: req.jwt._id }, (err, _redactor) => {
            if(err) return console.log("****************"+err);

            if( _redactor ){
                let redactorTests = _redactor.tests;
                for( var i=0; i<arrayTests.length; i++ ){
                    for( var j=0; j<redactorTests.length; j++){
                        if(arrayTests[i]._id.toString() == redactorTests[j]._id.toString()  ){
                            arrayTests[i].chances = redactorTests[j].chances.length;
                            arrayTests[i].approved = redactorTests[j].approved;
                            break;
                        }
                    }
                }
            }
            res.json( { tests: arrayTests} );
        });
    });
};




//=========================
exports.create_test = (req, res ) => {
    var test1 = new Test({
       title: 'Nivel 2',
       limit:0,
       badge: {
               image: 'https://s3-us-west-2.amazonaws.com/textoclick/common/badges/nivel02.png',
               image_blocked:'https://s3-us-west-2.amazonaws.com/textoclick/common/badges/nivel02.png',
               title: 'Redactor Nivel 2',
               description: 'Badge para los redactores de nivel 2'
           },
       preguntas: [
         {
           pregunta: 'Juan ____________ su jardín.',
           respuestas: [
             {
                 respuesta: 'halumbra'
             },
             {
                 respuesta: 'alhumbra'
             },
             {
                 respuesta: 'alumbra', correcto: true
             }
           ]
         },
         {
           pregunta: 'El ciervo tiene el __________ rota.',
           respuestas: [
             {
                 respuesta: 'ásta'
             },
             {
                 respuesta: 'hasta'
             },
             {
                 respuesta: 'asta ', correcto: true
             }
           ]
         },
         {
           pregunta: 'Yo siempre _________ bien antes de hornear.',
           respuestas: [
             {
                 respuesta: 'hablando'
             },
             {
                 respuesta: 'hablandó'
             },
             {
                 respuesta: 'ablando ', correcto: true
             }
           ]
         },
         {
           pregunta: 'No ______ manzanas, sólo tengo peras.',
           respuestas: [
             {
                 respuesta: 'ay'
             },
             {
                 respuesta: 'hay', correcto: true
             },
             {
                 respuesta: 'ahí'
             }
           ]
         },
         {
           pregunta: '¿Ves ese lugar? _________ es donde escondí el tesoro.',
           respuestas: [
             {
                 respuesta: 'Ay'
             },
             {
                 respuesta: 'Ahí', correcto: true
             },
             {
                 respuesta: 'Hay'
             }
           ]
         },
         {
           pregunta: 'Esa _____ fui muy feliz.',
           respuestas: [
             {
                 respuesta: 'Vez', correcto: true
             },
             {
                 respuesta: 'Ves'
             },
             {
                 respuesta: 'Bez'
             }
           ]
         },
         {
           pregunta: 'Todos quedamos ___________ con la carrera.',
           respuestas: [
             {
                 respuesta: 'exhaustos', correcto: true
             },
             {
                 respuesta: 'Exhaustos'
             },
             {
                 respuesta: 'exaustos'
             }
           ]
         },
         {
           pregunta: 'El avión no ____ podido despegar por una falla mecánica.',
           respuestas: [
             {
                 respuesta: 'ha', correcto: true
             },
             {
                 respuesta: 'ah'
             },
             {
                 respuesta: 'a'
             }
           ]
         },
         {
           pregunta: 'Los policias _______ al delincuente.',
           respuestas: [
             {
                 respuesta: 'aprehendieron', correcto: true
             },
             {
                 respuesta: 'aprendieron'
             },
             {
                 respuesta: 'haprendieron'
             }
           ]
         },
         {
           pregunta: 'Deja que el agua __________',
           respuestas: [
             {
                 respuesta: 'hierva', correcto: true
             },
             {
                 respuesta: 'hierba'
             },
             {
                 respuesta: 'hierbva'
             }
           ]
         },
         {
           pregunta: 'Ha crecido la _______ del terreno de enfrente.',
           respuestas: [
             {
                 respuesta: 'hierva'
             },
             {
                 respuesta: 'hierba', correcto: true
             },
             {
                 respuesta: 'hierbva'
             }
           ]
         },
         {
           pregunta: 'La __________ es uno de los valores más importantes de la sociedad.',
           respuestas: [
             {
                 respuesta: 'Solidalidad'
             },
             {
                 respuesta: 'Solidaridad', correcto: true
             },
             {
                 respuesta: 'Sodilaridad'
             }
           ]
         },
       ]
     });

      test1.save( (err, _test) => {
        if(err) return console.log(err);
        console.log('test creado');

        res.json({success:'test-create'});

      });


}
