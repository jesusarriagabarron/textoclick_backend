'use strict'

var jwt = require('../middleware/jwt');

module.exports = (app) => {

  //load User controller
  var article = require('../controllers/articleController');


  // Protected routes
  //================== REDACTOR METHODS
  app.route('/article/upload').post( jwt.validate_route, article.upload_article );
  app.route('/article/download/:id').get(jwt.validate_route,article.download_article_private);
  app.route('/article/categories').get( jwt.validate_route, article.get_categories );
  app.route('/writter/articles/').post( jwt.validate_route, article.get_my_articles );


  //Public Methods routes
  app.route('/article/:id').get(article.get_article);


}
