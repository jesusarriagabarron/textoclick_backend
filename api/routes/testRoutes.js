'use strict'

var jwt = require('../middleware/jwt');

module.exports = (app) => {
  //load Test controller
  var test = require('../controllers/testController');
  // Protected routes
  //================== TEST METHODS

  app.route('/test-create').get( test.create_test );
  app.route('/test/all').get( jwt.validate_route, test.all_test );
  app.route('/test/id/:id').get( jwt.validate_route, test.get_test );
  app.route('/test/start').post( jwt.validate_route, test.start_test );
  app.route('/test/finish').post( jwt.validate_route, test.finish_test );
}
