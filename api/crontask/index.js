'use strict'

const schedule    = require('node-schedule');
const Redactor          = require('../models/redactorModel');

exports.start = function(){
    let count = 0;
    var certificationTimer = schedule.scheduleJob("01 * * * *",function(){

        Redactor.find({
            'tests.initiate':true,
            'tests.approved':false
        },( err, _redactors ) => {
            if( _redactors ){
                _redactors.forEach( redactor => {
                    let testsObjects = redactor.tests.map(item => {
                        if(item.initiate==true && item.approved == false){
                            item.initiate=false;
                        }
                        return item;
                    });
                    redactor.tests = testsObjects;
                    redactor.save();
                });
            }
            console.log("restoring initiated test");
        });
    });
}
