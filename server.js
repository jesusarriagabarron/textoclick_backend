'use strict'
//=================== Load environment configuration
require('dotenv').config();
//=================== Modules
const express = require('express'),
  app         = express(),
  port        = process.env.port || 3000,
  bodyParser  = require('body-parser'),
  glob        = require('glob'),
  path        = require('path'),
  mongoose    = require('mongoose'),
  eValidator  = require('express-validator'),
  cors        = require('cors'),
  tasks       = require('./api/crontask');
  //jwt         = require('./api/middleware/jwt');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit:'20mb'}));
app.use(eValidator());
app.use(cors());

//==================== Mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_HOST,{ useMongoClient: true,user:process.env.DB_USER,pass:process.env.DB_PASS});

// =================== Automatically require all controllers
glob.sync('./api/routes/*.js').forEach((file) => {
  require(path.resolve(file))(app);
});
//==================== Start server listening
app.listen(port);

//==================== Response when the status is not found
app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

//===================== Schedule tasks
tasks.start();

console.log('RESTful API server started on: ' + port);
